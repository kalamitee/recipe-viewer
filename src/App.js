import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import RecipesList from './pages/RecipesList/RecipesList';
import RecipeDetails from './pages/RecipeDetails/RecipeDetails';
import AddRecipe from './pages/AddRecipe/AddRecipe';
import EditRecipe from './pages/EditRecipe/EditRecipe';
import NotFound from './pages/NotFound/NotFound';
import './App.css';

function App() {
  return (
    <div className="wrapper">
      <Router>
        <Switch>
          <Redirect exact from="/" to="/recipes" />
          <Route exact path="/recipes">
            <RecipesList />
          </Route>

          <Route exact path="/add-recipe">
            <AddRecipe />
          </Route>

          <Route exact path="/edit-recipe/:id">
            <EditRecipe />
          </Route>

          <Route exact path="/recipes/:id">
            <RecipeDetails />
          </Route>

          <Route path="*">
              <NotFound />
            </Route>
        </Switch>
      </Router>

      <footer className="footer">
          <Container className="text-right">A simple recipe viewer since 24 A.D.</Container>
      </footer>
    </div>
  );
}

export default App;
