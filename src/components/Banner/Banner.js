import React, { Component } from 'react';
import { Container, Jumbotron } from 'react-bootstrap';
import './Banner.css';

class Banner extends Component {
    renderContent() {
        if (this.props.children) {
            return <div className="banner__text">{this.props.children}</div>;
        }

        return (
            <div className="banner__text">
                <h1>Recipe book</h1>
                <p>A collection of delicious, but quick and easy to prepare meals</p>
            </div>
        );
    }

    render() {
        const imageUrl = this.props.bannerImage || 'url(/img/banner.jpg)'; // default banner: https://unsplash.com/photos/Ww8eQWjMJWk

        return (
            <Jumbotron className="banner" style={{backgroundImage: imageUrl}}>
                <Container>
                    {this.renderContent()}
                </Container>
            </Jumbotron>  
        );
    }
}

export default Banner;
