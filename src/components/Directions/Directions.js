import React, { Component } from 'react';
import { Card, Row, Col } from 'react-bootstrap';

class Directions extends Component {
    renderDirections() {
        if (!this.props.directions?.length) {
            return <h5>No instructions ¯\_(ツ)_/¯</h5>;
        }

        return this.props.directions.map((direction, index) => {
            return (
                <li className="mb-2" key={index}>
                    <span className="mr-2">{direction.instructions}</span>
                    {direction.optional ? <small><strong>(Optional)</strong></small> : null}
                </li>
            );
        });
    }

    render() {
        return (
            <Card bg="light">
                <Card.Header>
                    <Row>
                        <Col sm>How to cook</Col>
                        {
                            this.props.prepTime || this.props.cookTime ?
                            <Col sm>
                                <small className="text-muted mr-2"><strong>Prep:</strong> {this.props.prepTime} mins.</small>
                                <small className="text-muted"><strong>Cook:</strong> {this.props.cookTime} mins.</small>
                            </Col> : null
                        }
                    </Row>
                </Card.Header>
                <Card.Body>
                    <ol>{this.renderDirections()}</ol>
                </Card.Body>
            </Card>
        );
    }
}

export default Directions;
