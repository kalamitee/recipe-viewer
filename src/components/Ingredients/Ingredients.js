import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import './Ingredients.css';

class Ingredients extends Component {
    renderSpecial(ingredient) {
        const foundSpecial = this.props.specials.find((special) => special.ingredientId === ingredient.uuid);
        
        if (!foundSpecial) {
            return null;
        }
        
        return (
            <div className="ml-2">
                <small className="text-muted">{foundSpecial.title}</small><br />
                <small className="text-muted">{foundSpecial.text}</small><br />
                <small className="text-muted">{foundSpecial.type}</small>
            </div>
        );
    }

    renderIngredients() {
        if (!this.props.ingredients?.length) {
            return <h5>No ingredients ¯\_(ツ)_/¯</h5>;
        }

        return this.props.ingredients.map((ingredient, index) => {
            return (
                <div className="ingredient" key={index}>
                    <div>{`${ingredient.amount || ''} ${ingredient.measurement} ${ingredient.name}`}</div>
                    {this.renderSpecial(ingredient)}
                </div>
            );
        });
    }

    render() {
        return (
            <Card bg="light">
                <Card.Header>What you need</Card.Header>
                <Card.Body>{this.renderIngredients()}</Card.Body>
            </Card>
        );
    }
}

export default Ingredients;
