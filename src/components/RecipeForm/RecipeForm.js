import React, { Component } from 'react';
import { Col, Form, Row, Button, Card } from 'react-bootstrap';
import './RecipeForm.css'

class RecipeForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            cookTime: '',
            prepTime: '',
            ingredients: [this.getNewIngredient()],
            directions: [this.getNewDirection()]
        };
    }

    componentDidMount() {
        if (this.props.recipe) {
            this.setState({...this.props.recipe});
        }
    }

    getNewIngredient() {
        return {name: '', amount: '', measurement: ''};
    }

    getNewDirection() {
        return {instructions: '', optional: false};
    }

    handleChange(e) {
        const { name, value, type } = e.target;
        this.setState({[name]: type === 'number' ? +value : value});
    }

    handleArrayChange(e, arrayName, index) {
        const currentArray = this.state[arrayName].slice();
        const target = e.target;
        currentArray[index][target.name] = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({[arrayName]: currentArray});
    }

    addArrayItem(arrayName) {
        const currentArray = this.state[arrayName].slice();
        this.setState({[arrayName]: currentArray.concat(arrayName === 'ingredients' ? this.getNewIngredient() : this.getNewDirection())});
    }

    removeArrayItem(arrayName, index) {
        const currentArray = this.state[arrayName].slice();
        const newArr = [...currentArray.slice(0, index), ...currentArray.slice(index + 1)];

        if (!newArr.length) {
            newArr.push(arrayName === 'ingredients' ? this.getNewIngredient() : this.getNewDirection());
        }

        this.setState({[arrayName]: newArr});
    }

    renderDirections() {
        return this.state.directions.map((direction, index) => {
            return (
                <Card className="mb-3" key={index}>
                    <Card.Header className="button-container">
                        Instruction #{index + 1}
                        <Button variant="outline-danger" size="sm" onClick={(e) => this.removeArrayItem('directions', index)}>Remove</Button>
                    </Card.Header>
                    <Card.Body>
                        <Form.Group>
                            <Form.Control 
                                as="textarea"
                                rows={3}
                                name="instructions"
                                value={direction.instructions}
                                onChange={(e) => this.handleArrayChange(e, 'directions', index)}
                                required
                            ></Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Check 
                                type="checkbox" 
                                name="optional"
                                label="Optional" 
                                checked={direction.optional} 
                                onChange={(e) => this.handleArrayChange(e, 'directions', index)}
                            />
                        </Form.Group>
                    </Card.Body>
                </Card>
            );
        });
    }

    renderIngredients() {
        return this.state.ingredients.map((ingredient, index) => {
            return (
                <Card className="mb-3" key={index}>
                    <Card.Header className="button-container">
                        Ingredient #{index + 1}
                        <Button variant="outline-danger" size="sm" onClick={(e) => this.removeArrayItem('ingredients', index)}>Remove</Button>
                    </Card.Header>
                    <Card.Body>
                        <Form.Group>
                            <Form.Label>Name:</Form.Label>
                            <Form.Control 
                                type="text"
                                name="name"
                                value={ingredient.name}
                                onChange={(e) => this.handleArrayChange(e, 'ingredients', index)}
                                required
                            ></Form.Control>
                        </Form.Group>
                        <Form.Row>
                            <Form.Group as={Col} sm>
                                <Form.Label>Amount:</Form.Label>
                                <Form.Control 
                                    min={0}
                                    type="number"
                                    name="amount"
                                    step="any"
                                    value={ingredient.amount}
                                    onChange={(e) => this.handleArrayChange(e, 'ingredients', index)}
                                ></Form.Control>
                            </Form.Group>
                            <Form.Group as={Col} sm>
                                <Form.Label>Measurement:</Form.Label>
                                <Form.Control 
                                    type="text"
                                    name="measurement"
                                    value={ingredient.measurement}
                                    onChange={(e) => this.handleArrayChange(e, 'ingredients', index)}
                                ></Form.Control>
                            </Form.Group>
                        </Form.Row>
                    </Card.Body>
                </Card>
            );
        });
    }

    render() {
        return (
            <Form onSubmit={(e) => this.props.onSubmit(e, {...this.state})}>
                <Form.Group>
                    <Form.Label>Title:</Form.Label>
                    <Form.Control 
                        type="text"
                        name="title"
                        value={this.state.title}
                        onChange={(e) => this.handleChange(e)}
                        required
                    ></Form.Control>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Description:</Form.Label>
                    <Form.Control 
                        as="textarea"
                        rows={3}
                        name="description"
                        value={this.state.description}
                        onChange={(e) => this.handleChange(e)}
                    ></Form.Control>
                </Form.Group>

                <Form.Row>
                    <Form.Group as={Col}>
                        <Form.Label>Prep time (mins.):</Form.Label>
                        <Form.Control 
                            min={0}
                            step="any"
                            type="number"
                            name="prepTime"
                            value={this.state.prepTime}
                            onChange={(e) => this.handleChange(e)}
                        ></Form.Control>
                    </Form.Group>

                    <Form.Group as={Col}>
                        <Form.Label>Cook time (mins.):</Form.Label>
                        <Form.Control
                            min={0}
                            step="any"
                            type="number"
                            name="cookTime"
                            value={this.state.cookTime}
                            onChange={(e) => this.handleChange(e)}
                        ></Form.Control>
                    </Form.Group>
                </Form.Row>

                <fieldset>
                    <legend>Ingredients</legend>
                    {this.renderIngredients()}

                    <Row>
                        <Col className="button-container">
                            <Button className="ml-auto" type="button" variant="success" size="sm" onClick={() => this.addArrayItem('ingredients')}>Add ingredient</Button>
                        </Col>
                    </Row>
                </fieldset>

                <fieldset>
                    <legend>Instructions</legend>
                    {this.renderDirections()}

                    <Row>
                        <Col className="button-container">
                            <Button className="ml-auto" type="button" variant="success" size="sm" onClick={() => this.addArrayItem('directions')}>Add instruction</Button>
                        </Col>
                    </Row>
                </fieldset>

                <Button className="mt-3" type="submit" block>Save recipe</Button>
            </Form>
        );
    }
}

export default RecipeForm;
