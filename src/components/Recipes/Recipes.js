import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Card, Button } from 'react-bootstrap';
import './Recipes.css';

class Recipes extends Component {
    renderRecipes() {
        if (!this.props.recipes?.length) {
            return <Col md={3} sm={6}>No recipes available</Col>
        }

        return this.props.recipes.map((recipe) => {
            return (
                <Col md={4} sm={6} key={recipe.uuid}>
                    <Card style={{marginBottom: '1.5rem'}} bg="light">
                        {
                            recipe.images ? 
                                <Card.Img variant="top" src={recipe.images.small}></Card.Img> :
                                <Card.Header className="image-not-found text-center"><h5>IMAGE NOT AVAILABLE</h5></Card.Header>
                        }                        
                        <Card.Body>
                            <Card.Title>{recipe.title}</Card.Title>
                            <Card.Text>{recipe.description}</Card.Text>
                            <Button variant="outline-success" as={Link} to={`/recipes/${recipe.uuid}`} block>View recipe</Button>
                        </Card.Body>
                    </Card>
                </Col>
            );
        });
    }

    render() {
        return (
            <Row>
                {this.renderRecipes()}
            </Row>
        );
    }
}

export default Recipes;
