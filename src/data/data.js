import axios from 'axios';

const BASE_URL = 'http://localhost:3001';

export const getRecipes = () => {
    return axios.get(`${BASE_URL}/recipes?_sort=title`)
        .then((response) => response.data);
}

export const getRecipeById = (id) => {
    return axios.get(`${BASE_URL}/recipes/${id}`)
        .then((response) => response.data);
}

export const getSpecials = () => {
    return axios.get(`${BASE_URL}/specials`)
        .then((response) => response.data);
}

export const saveRecipe = (recipe) => {
    console.log('RECIPE ', recipe.uuid);

    if (!recipe.uuid) {
        return axios.post(`${BASE_URL}/recipes`, recipe)
            .then((response) => response.data);
    }
    
    console.log('RECIPE ', recipe.uuid);

    return axios.patch(`${BASE_URL}/recipes/${recipe.uuid}`, recipe)
        .then((response) => response.data);
}
