import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { withRouter, Link } from 'react-router-dom';
import { saveRecipe } from '../../data/data';
import Banner from '../../components/Banner/Banner';
import RecipeForm from '../../components/RecipeForm/RecipeForm';
import './AddRecipe.css';

class AddRecipe extends Component {
    async handleSubmit(e, recipe) {
        e.preventDefault();
        await saveRecipe(recipe);
        this.props.history.push('/recipes');
    }

    render() {
        return (
            <div>
                <Banner>
                    <h2>Adding yummy recipe!</h2>
                </Banner>

                <Container className="add-form-container mb-4">
                    <Row className="mb-3">
                        <Col className="text-right">
                            <Button variant="info" as={Link} to="/recipes">Go to recipes</Button>
                        </Col>
                    </Row>
                    <RecipeForm onSubmit={(e, recipe) => this.handleSubmit(e, recipe)} />
                </Container>
            </div>
        );
    }
}

export default withRouter(AddRecipe);
