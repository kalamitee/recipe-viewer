import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { saveRecipe, getRecipeById } from '../../data/data';
import RecipeForm from '../../components/RecipeForm/RecipeForm';
import Banner from '../../components/Banner/Banner';
import './EditRecipe.css';

class EditRecipe extends Component {
    constructor (props) {
        super(props);
        this.state = {
            recipe: {},
            isLoading: true
        }
    }

    componentDidMount() {
        console.log('Props' , this.props);
        this.loadRecipe();
    }

    async loadRecipe() {
        this.setState({isLoading: true});

        try {
            this.setState({recipe: await getRecipeById(this.props.match.params.id)})
        } catch (error) {
            console.log('ERROR ', error);
        } finally {
            this.setState({isLoading: false});
        }
    }

    async handleSubmit(e, recipe) {
        e.preventDefault();
        await saveRecipe(recipe);
        this.props.history.push(`/recipes/${recipe.uuid}`);
    }

    render() {
        return (
            <div>
                <Banner>
                    <h2>Update recipe</h2>
                </Banner>

                <Container className="edit-recipe-container mb-4">
                    <Row className="mb-3">
                        <Col className="text-right">
                            <Button variant="info" as={Link} to={`/recipes/${this.props.match.params.id}`}>View recipe</Button>
                        </Col>
                    </Row>
                    {
                        this.state.isLoading ? <h3>Loading...</h3>
                            : <RecipeForm recipe={this.state.recipe} onSubmit={(e, recipe) => this.handleSubmit(e, recipe)} />
                    }
                </Container>
            </div>
        );
    }
}

export default withRouter(EditRecipe);
