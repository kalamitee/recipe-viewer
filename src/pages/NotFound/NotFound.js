import React, { Component } from 'react';
import { Container, Col, Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Banner from '../../components/Banner/Banner';
import './NotFound.css'

class NotFound extends Component {
    render() {
        return (
            <div>
                <Banner>
                    <h1>Oooops!</h1>
                </Banner>

                <Container className="message">
                    <Row className="justify-content-md-center">
                        <Col md="auto">
                            <h1>404 Not found</h1>
                            <h3>Sorry, this page is not cooked yet</h3>
                            <Button variant="info" as={Link} to="/recipes">Go to recipes</Button>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default NotFound;
