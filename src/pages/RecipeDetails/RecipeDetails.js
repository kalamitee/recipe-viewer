import React, { Component } from 'react';
import axios from 'axios';
import { withRouter, Link } from 'react-router-dom';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { getRecipeById, getSpecials } from '../../data/data';
import Ingredients from '../../components/Ingredients/Ingredients';
import Directions from '../../components/Directions/Directions';
import Banner from '../../components/Banner/Banner';

class RecipeDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recipe: {},
            specials: [],
            isLoading: true,
        };
    }

    componentDidMount() {
        this.loadData();
    }

    async loadData() {
        this.setState({isLoading: true});
        try {
            const [recipe, specials] = await axios.all([
                getRecipeById(this.props.match.params.id),
                getSpecials()
            ]);
            
            this.setState({recipe, specials});
        } catch (error) {
            console.log('THE ERROR ', error);
        } finally {
            this.setState({isLoading: false});
        }
    }

    render() {
        const image = this.state.recipe.images ? `url(${this.state.recipe.images.full})` : null;

        return (
            <div>
                <Banner bannerImage={image}>
                    <h2>{this.state.isLoading ? 'Loading...' : this.state.recipe.title}</h2>
                </Banner>

                <Container>
                    <Row className="mb-3">
                        <Col className="text-right">
                            <Button className="mr-2" variant="info" as={Link} to="/recipes">Go to recipes</Button>
                            <Button variant="success" as={Link} to={`/edit-recipe/${this.state.recipe.uuid}`}>Edit this recipe</Button>
                        </Col>
                    </Row>
                    <Row className="mb-3">
                        <Col className="mb-3" sm>
                            <Ingredients 
                                ingredients={this.state.recipe.ingredients}
                                specials={this.state.specials}
                            />
                        </Col>
                        <Col sm>
                            <Directions
                                directions={this.state.recipe.directions}
                                cookTime={this.state.recipe.cookTime}
                                prepTime={this.state.recipe.prepTime}
                            />
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default withRouter(RecipeDetails);
