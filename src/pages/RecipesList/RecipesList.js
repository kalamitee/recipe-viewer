import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { getRecipes } from '../../data/data';
import Recipes from '../../components/Recipes/Recipes';
import Banner from '../../components/Banner/Banner';

class RecipesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recipes: [],
            isLoading: true
        };
    }

    componentDidMount() {
        this.loadRecipes();
    }

    async loadRecipes() {
        this.setState({isLoading: true});

        try {
            this.setState({recipes: await getRecipes()});
        } catch (error) {
            console.log(error);
        } finally {
            this.setState({isLoading: false});
        }
    }

    render() {
        return (
            <div>
                <Banner />
                <Container>
                    <Row className="mb-3">
                        <Col className="text-right">
                            <Button variant="success" as={Link} to="/add-recipe">Add recipe</Button>
                        </Col>
                    </Row>
                    {this.state.isLoading ? <h3>Loading...</h3> : <Recipes recipes={this.state.recipes} />}
                </Container>
            </div>
        );
    }
}

export default RecipesList;
